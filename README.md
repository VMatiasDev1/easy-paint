<h1 align='center'>Easy Paint</h1>

Aplicação para ajudar quem precisa pintar o uma sala e pode simular tamanho de paredes e quantas janelas e portas existem em cada uma.

[Acesse a aplicação](https://easy-paint.vercel.app//)

<h2 align='center'>Rodando o projeto localmente</h2>

<h3>Pré-Requisitos:</h3>
NodeJs

Yarn

<h3>Instalação:</h3>

1. Clonar o repositório

```sh
   git clone https://gitlab.com/VMatiasDev1/easy-paint.git
```

2. Instalar as dependências locais

```sh
   yarn
```

3. Rodar o projeto

```sh
   yarn dev
```

<h2 align='center'>Tecnologias Utilizadas</h2>
<div>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/react/react-original-wordmark.svg" width='40' heigth='40' alt='react logo'/>
  <img src="https://cdn.jsdelivr.net/gh/devicons/devicon/icons/javascript/javascript-original.svg" width='40' height='40' alt='javascript logo'/>
  <img src="https://styled-components.com/logo.png" width='40' height='40' alt='styled components logo'/>
</div>

<h2 align='center'>Equipe responsável</h2>

<table>
  <tr align="center">
    <td>
      <a href="https://github.com/VMatiasDev" target="_blank">
        <img src="https://avatars.githubusercontent.com/u/109880266?v=4" height="150px">
      </a>
    </td>
  </tr>
  <tr align="center">
    <td>
      <a href="https://github.com/VMatiasDev" target="_blank">Vitor Matias (Dev)</a>
    </td>
  </tr>
</table>
