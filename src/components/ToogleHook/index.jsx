import { useState } from 'react';
import { ConstructWallContainer } from '../EachWall/style';
import { ToggleButton } from './style';

export function ToogleHook({ whatWillToggle }) {
  const useToggle = (initialState) => {
    const [toggleValue, setToggleValue] = useState(initialState);

    const toggler = () => {
      setToggleValue(!toggleValue);
    };
    return [toggleValue, toggler];
  };

  const [toggle, setToggle] = useToggle();

  return (
    <ConstructWallContainer
      style={{ background: 'transparent', border: 'none' }}
    >
      <ToggleButton onClick={setToggle}>{toggle ? 'X' : 'Abra'}</ToggleButton>
      {toggle && whatWillToggle}
    </ConstructWallContainer>
  );
}
