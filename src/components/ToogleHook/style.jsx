import styled from 'styled-components';

export const ToggleButton = styled.button`
  border: 2px solid #f6921e;
  border-radius: 10px;
  background-color: #111111;
  padding: 10px;
  cursor: pointer;
  color: #f6921e;
  font-weight: bold;
`;
