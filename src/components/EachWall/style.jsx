import styled from 'styled-components';

// colors based on digital republic

export const ConstructWallContainer = styled.div`
  width: 400px;
  height: 400px;
  border: 2px solid #f6921e;
  border-radius: 10px;
  background-color: #111111;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
  @media (max-width: 768px) {
    width: 300px;
    height: 300px;
  }
`;

export const Title = styled.h1`
  background-color: transparent;
  color: #f6921e;
  display: flex;
  justify-content: center;
  @media (max-width: 768px) {
    font-size: 1.5rem;
  }
`;

export const InputContainer = styled.div`
  background-color: transparent;
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 5px;
`;

// style based on digital republic form input
export const Input = styled.input.attrs({
  type: 'number',
  placeholder: 'Atribua um número',
})`
  background-color: transparent;
  border: 0;
  border-bottom: 1px solid #fff;
  outline: 0;
  color: #f6921e;
  width: 50%;
  // removes spinner - chrome, safari, edge, opera
  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  -moz-appearance: textfield; /* removes spinner - firefox */
`;

export const Label = styled.p`
  font-weight: bold;
  background-color: transparent;
  color: #f1471f;
  width: 50%;
  @media (max-width: 768px) {
    font-size: 0.8rem;
  }
`;

export const Result = styled.p`
  background-color: transparent;
  color: #f6921e;
  display: flex;
  justify-content: center;
`;
