import { useEffect, useState } from 'react';

import {
  ConstructWallContainer,
  Title,
  Input,
  Label,
  InputContainer,
  Result,
} from './style';

export function EachWall({ setArea, setInk }) {
  const [width, setWidth] = useState();
  const [height, setHeight] = useState();
  const [windowsQtd, setWindowsQtd] = useState();
  const [doorsQtd, setDoorsQtd] = useState();
  const [inkNeeded, setInkNeeded] = useState();
  const [paintableArea, setPaintableArea] = useState();
  const [invalidWall, setInvalidWall] = useState();

  useEffect(() => {
    width === undefined ||
    height === undefined ||
    windowsQtd === undefined ||
    doorsQtd === undefined
      ? console.log('Falta setar algum input')
      : doMathAndValidateRules();
  }, [width, height, windowsQtd, doorsQtd]);

  const doorArea = 1.52;
  const windowArea = 2.4;
  const metersPaintedBy1Liter = 5;

  const doMathAndValidateRules = () => {
    const wallArea = areaWall(width, height);
    const windowsArea = sumAreas(windowsQtd, windowArea);
    const doorsArea = sumAreas(doorsQtd, doorArea);
    if (
      (doorsQtd >= 1 && height < 2.2) ||
      wallArea < 1 ||
      wallArea > 50 ||
      windowsArea + doorsArea > wallArea / 2
    ) {
      setInvalidWall('Parede inválida');
    } else {
      setInvalidWall();

      // ceiling paintable area, automatically ceil ink needed. It`s a safety margin to not disappoint costumer
      const paintableArea = Math.ceil(wallArea - windowsArea - doorsArea);
      setPaintableArea(`${paintableArea}`);

      const inkNeeded = howMuchInkNeed(paintableArea, metersPaintedBy1Liter);
      setInkNeeded(`${inkNeeded}`);

      setArea(paintableArea);
      setInk(inkNeeded);
    }
  };

  const howMuchInkNeed = (paintableArea, meterByLiter) =>
    paintableArea / meterByLiter;

  const areaWall = (width, height) => width * height;

  const sumAreas = (quantity, area) => quantity * area;

  // partial application that makes the handleInputChanges parameter on onChange run as fn(event.target.value)
  const handleInputChanges = (fn) => (event) => {
    fn(event.target.value);
  };

  return (
    <ConstructWallContainer>
      <Title>Construa sua parede</Title>
      <InputContainer>
        <Label>Largura da Parede</Label>
        <Input
          type="number"
          onChange={handleInputChanges(setWidth)}
          required
        ></Input>
      </InputContainer>
      <InputContainer>
        <Label>Altura da Parede</Label>
        <Input
          type="number"
          onChange={handleInputChanges(setHeight)}
          required
        ></Input>
      </InputContainer>
      <InputContainer>
        <Label>Quantidade de Janelas</Label>
        <Input
          type="number"
          onChange={handleInputChanges(setWindowsQtd)}
          required
        ></Input>
      </InputContainer>
      <InputContainer>
        <Label>Quantidade de Portas</Label>
        <Input
          type="number"
          onChange={handleInputChanges(setDoorsQtd)}
          required
        ></Input>
      </InputContainer>
      {!invalidWall && paintableArea != undefined && inkNeeded != undefined && (
        <>
          <Result>{`Essa parede tem ${paintableArea}m² de area à pintar`}</Result>
          <Result>{`Essa parede precisa de ${inkNeeded}L de tinta`}</Result>
        </>
      )}
      {invalidWall && <Result>{invalidWall}</Result>}
    </ConstructWallContainer>
  );
}
