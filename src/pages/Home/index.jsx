import { Title } from '../../components/EachWall/style';
import { ToogleHook } from '../../components/ToogleHook';
import { EachWall } from '../../components/EachWall';
import { FinalValuesContainer, WallsContainer, Wrapper } from './style';
import { useState } from 'react';
import { useEffect } from 'react';

export function Home() {
  const [area1, setArea1] = useState(0);
  const [area2, setArea2] = useState(0);
  const [area3, setArea3] = useState(0);
  const [area4, setArea4] = useState(0);
  const [ink1, setInk1] = useState(0);
  const [ink2, setInk2] = useState(0);
  const [ink3, setInk3] = useState(0);
  const [ink4, setInk4] = useState(0);
  const [usedCans, setUsedCans] = useState([]);

  const sumArea = area1 + area2 + area3 + area4;
  const sumInk = ink1 + ink2 + ink3 + ink4;
  useEffect(() => {
    defineCansNeeded();
  }, [sumInk]);

  const inkHalfLiter = 0.5;
  const ink2DotHalfLiter = 2.5;
  const ink3DotSixLiter = 3.6;
  const ink18Liter = 18;

  const defineCansNeeded = () => {
    let inkRemaining = sumInk;
    let whatCans = [];

    while (inkRemaining > 0) {
      if (inkRemaining >= ink18Liter) {
        inkRemaining -= ink18Liter;
        whatCans.push('Lata de 18L, ');
      } else if (inkRemaining >= ink3DotSixLiter) {
        inkRemaining -= ink3DotSixLiter;
        whatCans.push('Lata de 3.6L, ');
      } else if (inkRemaining >= ink2DotHalfLiter) {
        inkRemaining -= ink2DotHalfLiter;
        whatCans.push('Lata de 2.5L, ');
      } else if (inkRemaining >= inkHalfLiter) {
        inkRemaining -= inkHalfLiter;
        whatCans.push('Lata de 0.5L, ');
      } else if (inkRemaining < inkHalfLiter) {
        inkRemaining = 0;
        whatCans.push('Lata de 0.5L');
      }
    }
    setUsedCans(whatCans);
  };

  return (
    <Wrapper>
      <Title>Atribua os valores e descubra quais latas de tinta comprar</Title>
      <WallsContainer>
        <ToogleHook
          whatWillToggle={
            <EachWall
              setArea={setArea1}
              setInk={setInk1}
              defineCansNeeded={defineCansNeeded}
            />
          }
        />
        <ToogleHook
          whatWillToggle={
            <EachWall
              setArea={setArea2}
              setInk={setInk2}
              defineCansNeeded={defineCansNeeded}
            />
          }
        />
        <ToogleHook
          whatWillToggle={
            <EachWall
              setArea={setArea3}
              setInk={setInk3}
              defineCansNeeded={defineCansNeeded}
            />
          }
        />
        <ToogleHook
          whatWillToggle={
            <EachWall
              setArea={setArea4}
              setInk={setInk4}
              defineCansNeeded={defineCansNeeded}
            />
          }
        />
      </WallsContainer>
      <FinalValuesContainer>
        <Title>
          Você tem {sumArea}m² para pintar e precisa de {sumInk}L de tinta
        </Title>
        <Title>Recomendamos que compre: {usedCans}</Title>
      </FinalValuesContainer>
    </Wrapper>
  );
}
