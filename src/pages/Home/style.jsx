import styled from 'styled-components';

export const Wrapper = styled.main`
  display: flex;
  flex-wrap: wrap;
  height: 100vh;
  justify-content: space-evenly;
  align-items: center;
`;

export const WallsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 50px;
`;

export const FinalValuesContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
`;
