import { Route, Routes } from 'react-router-dom';
import { DefaultLayout } from '../../layout/DefaultLayout';
import { Home } from '../pages/Home';
import { NotFound } from '../pages/NotFound';

export function AppRoutes() {
  return (
    <Routes>
      <Route path="/" element={<DefaultLayout />}>
        <Route path="/" element={<Home />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
}
